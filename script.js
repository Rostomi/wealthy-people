let data = [];

let personsQuantity = 3;
for (let i = 0; i < personsQuantity; i++) {
  getRandomUser();
}

async function getRandomUser() {
  let apiUrl = "https://randomuser.me/api";
  const res = await fetch(apiUrl);
  const data = await res.json();

  const user = data.results[0];
  const newUser = {
    name: `${user.name.first} ${user.name.last}`,
    money: Math.floor(Math.random() * 10e6),
  };

  addData(newUser);
}

function addData(obj) {
  data.push(obj);

  updateDom();
}

function updateDom(providedData = data) {
  main.innerHTML = "<h2><strong>Person</strong> Wealth</h2>";

  providedData.forEach((item) => {
    const element = document.createElement("div");
    element.classList.add("person");
    element.innerHTML = ` <strong>${item.name}</strong> ${formatMoney(
      item.money
    )}`;
    main.appendChild(element);
  });

  calculate_wealth.disabled = false;
}

function formatMoney(num) {
  return "$ " + num.toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&,");
}

function doubleMoney() {
  data = data.map((user) => {
    return { ...user, money: user.money * 2 };
  });

  updateDom();
}

function sortByRichest() {
  data.sort((a, b) => b.money - a.money);

  updateDom();
}

function sortByPoorest() {
  data.sort((a, b) => a.money - b.money);

  updateDom();
}

function sortByAccending() {
  data.sort((a, b) => {
    if (a.name < b.name) {
      return -1;
    }
    if (a.name > b.name) {
      return 1;
    }
    return 0;
  });

  updateDom();
}

function sortByDeccending() {
  data.sort((a, b) => {
    if (a.name < b.name) {
      return 1;
    }
    if (a.name > b.name) {
      return -1;
    }
    return 0;
  });

  updateDom();
}

function ShowMills() {
  data = data.filter((items) => items.money > 1000000);

  updateDom();
}

function calculateSum() {
  const wealth = data.reduce((acc, sum) => (acc += sum.money), 0);

  const wealtElem = document.createElement("div");

  wealtElem.innerHTML = `<h3>Total: <strong>${formatMoney(
    wealth
  )}</strong></h3>`;

  main.appendChild(wealtElem);

  calculate_wealth.disabled = true;
}

add_user.addEventListener("click", getRandomUser);
double_money.addEventListener("click", doubleMoney);
sort_by_richest.addEventListener("click", sortByRichest);
sort_by_poorest.addEventListener("click", sortByPoorest);
sort_by_accend.addEventListener("click", sortByAccending);
sort_by_deccend.addEventListener("click", sortByDeccending);
show_milionaires.addEventListener("click", ShowMills);
calculate_wealth.addEventListener("click", calculateSum);
